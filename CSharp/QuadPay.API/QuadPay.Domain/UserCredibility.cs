﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuadPay.Domain
{
    public class UserCredibility
    {
        public UserCredibility()
        {
            OnTimePaymentRatio = -1;
            PastDueRatio = -1;
            OutstandingBalance = 0;
        }

        public float OnTimePaymentRatio { get; set; }
        public float PastDueRatio { get; set; }
        public decimal OutstandingBalance { get; set; }
    }
}
