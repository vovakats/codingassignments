﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuadPay.Domain
{
    //TODO: Inject Dao
    public class UserService : IUserService
    {
        public UserCredibility GetUserCredibility(long userId)
        {
            var paymentPlans = GetPaymentPlansByUser(userId);
            var latePaidPayments = 0;
            var allPaidPayments = 0;
            var pastDuePayments = 0;
            var allDuePayments = 0;
            var outstandingBalance = 0M;
            var credibility = new UserCredibility();

            foreach (var paymentPlan in paymentPlans)
            {
                foreach (var installment in paymentPlan.Installments)
                {
                    if (installment.IsPaid)
                    {
                        if (installment.SettlementDate > installment.Date)
                        {
                            latePaidPayments++;
                        }
                        allPaidPayments++;
                    }
                    else
                    {
                        outstandingBalance += installment.Amount;
                    } 

                    if (installment.Date > DateTime.UtcNow.Date)
                    {
                        if (!installment.IsPaid)
                        {
                            pastDuePayments++;
                        }
                        allDuePayments++;
                    }
                }
            }

            if (allPaidPayments > 0)
            {
                credibility.OnTimePaymentRatio = 1 - (latePaidPayments / allPaidPayments);
            }

            if (allDuePayments > 0)
            {
                credibility.OnTimePaymentRatio = 1 - (pastDuePayments / allDuePayments);
            }
            return credibility;
        }

        //TODO: get data from storage.
        private List<PaymentPlan> GetPaymentPlansByUser(long userId)
        {
            var paymentPlans = new List<PaymentPlan>();

            return paymentPlans;
        }
    }
}
