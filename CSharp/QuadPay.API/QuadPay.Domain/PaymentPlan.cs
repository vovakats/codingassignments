﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuadPay.Domain
{
    public class PaymentPlan
    {
        private decimal _amount;

        public Guid Id { get; private set; }
        public List<Installment> Installments { get; }
        public IList<Refund> Refunds { get; }
        public DateTime OriginationDate { get; }
        public PaymentPlan(decimal amount, int installmentCount = 4, int installmentIntervalDays = 14)
        {
            if (IsInputValid(amount, installmentCount, installmentIntervalDays))
            {
                this._amount = amount;
                Id = Guid.NewGuid();
                Installments = InitializeInstallments(amount, installmentCount, installmentIntervalDays);
                OriginationDate = DateTime.UtcNow.Date;
            }
            else
            {
                //using generic exception for now, could be custom exception if needed
                throw new ArgumentException();
            }
        }

        // Installments are paid in order by Date
        public Installment NextInstallment()
        {
            return Installments
                .FirstOrDefault(i => !i.IsPaid);
        }

        public Installment FirstInstallment()
        {
            return Installments.First();
        }

        public decimal OustandingBalance()
        {
            return Installments
                .Where(i => !i.IsPaid)
                .Sum(i => i.Amount);
        }

        public decimal AmountPastDue(DateTime currentDate)
        {
            return Installments
                .Where(i => !i.IsPaid && i.Date > currentDate)
                .Sum(i => i.Amount);
        }

        public IList<Installment> PaidInstallments()
        {
            return Installments
                .Where(i => i.IsPaid)
                .ToList();
        }

        public IList<Installment> DefaultedInstallments()
        {
            return Installments
                .Where(i => i.IsDefaulted)
                .ToList();
        }

        public IList<Installment> PendingInstallments()
        {
            return Installments
                .Where(i => i.IsPending)
                .ToList();
        }

        //I am not sure what this is.  Is it the original amount?  Or amount that has been paid by customer this far?
        public decimal MaximumRefundAvailable()
        {
            return _amount;
        }

        // We only accept payments matching the Installment Amount.
        //not sure why we need installment id here.  I would think we just apply amount to the next pending installement
        public void MakePayment(decimal amount, Guid installmentId)
        {
            if (IsPaymentValid(amount, installmentId))
            {
                var installment = Installments
                .Where(i => i.Id == installmentId)
                .First();

                installment.SetPaid("xxxxx");
            }
            else
            {
                throw new ArgumentException();
            }
        }

        // Returns: Amount to refund via PaymentProvider
        public decimal ApplyRefund(Refund refund)
        {
            if (IsRefundValid(refund.Amount))
            {
                decimal cashRefund = 0;
                var amount = refund.Amount;

                foreach (Installment installment in Installments)
                {
                    if (amount >= installment.Amount)
                    {
                        if (installment.IsPaid)
                        {
                            cashRefund += installment.Amount;
                        }
                        else
                        {
                            installment.SetPaid("xxxxx");
                        }
                        amount -= installment.Amount;
                    }
                    else if (amount > 0)
                    {
                        if (installment.IsPaid)
                        {
                            cashRefund += amount;
                        }
                        else
                        {
                            installment.Amount -= amount;
                        }
                        amount = 0;
                        break;
                    }
                }

                return cashRefund;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public void ChangeInstallmentPlan(int installmentCount, int installmentIntervalDays = 14)
        {
            var amount = OustandingBalance();
            var newInstallments = InitializeInstallments(amount, installmentCount, installmentIntervalDays);

            Installments.RemoveAll(i => !i.IsPaid);
            Installments.AddRange(newInstallments);
        }

        private bool IsInputValid(decimal amount, int installmentCount, int installmentIntervalDays)
        {
            //Is there a minimum amount?  for now keeping it at >= 1 cent
            if (!(amount >= 0.01M)) return false;
            //I assume we would want more then 1 installment
            if (!(installmentCount > 1)) return false;
            if (!(installmentIntervalDays > 0)) return false;
            return true;
        }

        private bool IsRefundValid(decimal amount)
        {
            if (amount <= _amount && amount >= 0.01M)
                return true;
            return false;
        }

        private bool IsPaymentValid(decimal amount, Guid installmentId)
        {
            var installment = Installments
                .Where(i => i.Id == installmentId)
                .FirstOrDefault();

            if (installment != null)
            {
                if (installment.Amount == amount)
                    return true;
            }

            return false;
        }

        // First Installment always occurs on PaymentPlan creation date
        private List<Installment> InitializeInstallments(decimal amount, int installmentCount, int installmentIntervalDays)
        {
            var installments = new List<Installment>();

            var tempInstallmentAmount = amount / installmentCount;
            var installmentAmount = Math.Round(tempInstallmentAmount, 2);
            var tempAmount = installmentAmount * installmentCount;
            var delta = amount - tempAmount;

            for (int i = 0; i < installmentCount; i++)
            {
                var installment = new Installment();
                installment.Amount = installmentAmount;
                installment.Id = Guid.NewGuid();
                //not sure about the time portion of the date.  Should it be set to a certain hour?  
                installment.Date = DateTime.UtcNow.AddDays(installmentIntervalDays * i).Date;
                installments.Add(installment);
            }

            //adjust installment amount
            if (delta > 0)
            {
                installments.First().Amount = installments.First().Amount + delta;
            }
            else if (delta < 0)
            {
                installments.Last().Amount = installments.Last().Amount + delta;
            }

            return installments;
        }
    }
}