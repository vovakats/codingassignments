using System;

namespace QuadPay.Domain
{
    public class Installment
    {
        private InstallmentStatus _installmentStatus;
        //test
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        //not sure what this is used for
        public string PaymentReference { get; private set; }
        // Date this Installment was marked 'Paid'
        public DateTime SettlementDate { get; private set; }

        public Installment() { }

        public bool IsPaid
        {
            get
            {
                return _installmentStatus == InstallmentStatus.Paid ? true : false; ;
            }
        }

        public bool IsDefaulted
        {
            get
            {
                return _installmentStatus == InstallmentStatus.Defaulted ? true : false;
            }
        }

        public bool IsPastDue
        {
            get
            {
                if (IsPaid)
                    return false;
                if (DateTime.UtcNow.Date < Date)
                    return true;
                return false;
            }
        }

        public bool IsPending
        {
            get
            {
                return _installmentStatus == InstallmentStatus.Pending ? true : false;
            }
        }

        public void SetPaid(string paymentReference)
        {
            _installmentStatus = InstallmentStatus.Paid;
            PaymentReference = paymentReference;
            SettlementDate = DateTime.UtcNow.Date;
        }
    }

    public enum InstallmentStatus
    {
        Pending = 0, // Not yet paid
        Paid = 1, // Can be either paid with a charge, or covered by a refund
        Defaulted = 2 // Charge failed
    }
}