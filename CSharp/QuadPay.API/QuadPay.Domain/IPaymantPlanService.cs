﻿namespace QuadPay.Domain
{
    public interface IUserService
    {
        UserCredibility GetUserCredibility(long userId);
    }
}