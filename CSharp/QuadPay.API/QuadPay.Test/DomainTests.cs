using System;
using Xunit;
using QuadPay.Domain;
using System.Linq;

namespace QuadPay.Test
{
    public class DomainTests
    {

        [Theory]
        [InlineData(-100, 4, 2)]
        [InlineData(123.23, 0, 2)]
        [InlineData(.001, 2, 2)]
        public void ShouldThrowExceptionForInvalidParameters(decimal amount, int installmentCount, int installmentIntervalDays)
        {
            Assert.Throws<ArgumentException>(() => {
                var paymentPlan = new PaymentPlan(amount, installmentCount, installmentIntervalDays);
            });
        }

        [Theory]
        [InlineData(1000, 4, 2)]
        [InlineData(120.28, 2, 2)]
        public void ShouldCreateCorrectNumberOfInstallmentsWithSameAmounts(decimal amount, int installmentCount, int installmentIntervalDays)
        {
            var paymentPlan = new PaymentPlan(amount, installmentCount, installmentIntervalDays);
            Assert.Equal(installmentCount, paymentPlan.Installments.Count);
            Assert.Equal(paymentPlan.Installments.First().Amount, paymentPlan.Installments.Last().Amount);
        }

        [Theory]
        [InlineData(1001.01, 4, 2)]
        [InlineData(123.23, 2, 2)]
        [InlineData(.01, 4, 2)]
        public void ShouldCreateCorrectNumberOfInstallmentsWithDiffAmounts(decimal amount, int installmentCount, int installmentIntervalDays)
        {
            var paymentPlan = new PaymentPlan(amount, installmentCount, installmentIntervalDays);
            Assert.Equal(installmentCount, paymentPlan.Installments.Count);
            Assert.NotEqual(paymentPlan.Installments.First().Amount, paymentPlan.Installments.Last().Amount);
        }

        [Fact]
        public void ShouldReturnCorrectAmountToRefundAgainstPaidInstallments()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            var cashRefundAmount = paymentPlan.ApplyRefund(new Refund(Guid.NewGuid().ToString(), 100));
            Assert.Equal(25, cashRefundAmount);
            Assert.Equal(0, paymentPlan.OustandingBalance());
        }

        [Theory]
        [InlineData(120.00)]
        [InlineData(-10)]
        [InlineData(0)]
        public void ShouldThrowExceptionForInvalidRefundAmount(decimal amount)
        {
            var paymentPlan = new PaymentPlan(100, 4);
            Assert.Throws<ArgumentException>(() =>
            {
                paymentPlan.ApplyRefund(new Refund(Guid.NewGuid().ToString(), amount));
            });
        }

        [Fact]
        public void PartialRefundShouldReturnCorrectAmountToRefundAgainstPaidInstallments1()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            var secondInstallment = paymentPlan.NextInstallment();
            paymentPlan.MakePayment(25, secondInstallment.Id);
            var cashRefundAmount = paymentPlan.ApplyRefund(new Refund(Guid.NewGuid().ToString(), 30));
            Assert.Equal(30, cashRefundAmount);
            Assert.Equal(50, paymentPlan.OustandingBalance());
        }

        [Fact]
        public void PartialRefundShouldReturnCorrectAmountToRefundAgainstPaidInstallments2()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            var cashRefundAmount = paymentPlan.ApplyRefund(new Refund(Guid.NewGuid().ToString(), 30));
            Assert.Equal(25, cashRefundAmount);
            Assert.Equal(70, paymentPlan.OustandingBalance());
        }

        [Fact]
        public void ShouldReturnCorrectOutstandingBalance()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            var secondInstallment = paymentPlan.NextInstallment();
            paymentPlan.MakePayment(25, secondInstallment.Id);
            Assert.Equal(50, paymentPlan.OustandingBalance());
        }

        [Fact]
        public void ShouldReturnCorrectPaidInstallments()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            var secondInstallment = paymentPlan.NextInstallment();
            paymentPlan.MakePayment(25, secondInstallment.Id);
            Assert.Equal(2, paymentPlan.PaidInstallments().Count);
        }

        [Fact]
        public void ShouldThrowExceptionForInvalidPaymentAmount()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            Assert.Throws<ArgumentException>(() =>
            {
                paymentPlan.MakePayment(10, paymentPlan.FirstInstallment().Id);
            });
        }

        [Fact]
        public void ShouldReturnCorrectInstallmentsAfterInstallmentChange()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            paymentPlan.ChangeInstallmentPlan(6);
            Assert.Equal(75, paymentPlan.OustandingBalance());
            Assert.Equal(6, paymentPlan.Installments.Count(i => !i.IsPaid));
        }

        [Fact]
        public void ShouldReturnCorrectAmountToRefundAfterInstallmentChangeAndRefund()
        {
            var paymentPlan = new PaymentPlan(100, 4);
            var firstInstallment = paymentPlan.FirstInstallment();
            paymentPlan.MakePayment(25, firstInstallment.Id);
            var secondInstallment = paymentPlan.NextInstallment();
            paymentPlan.MakePayment(25, secondInstallment.Id);
            paymentPlan.ChangeInstallmentPlan(5);
            Assert.Equal(50, paymentPlan.OustandingBalance());
            Assert.Equal(5, paymentPlan.Installments.Count(i => !i.IsPaid));
            var thirdInstallment = paymentPlan.NextInstallment();
            paymentPlan.MakePayment(10, thirdInstallment.Id);
            var cashRefundAmount = paymentPlan.ApplyRefund(new Refund(Guid.NewGuid().ToString(), 70));
            Assert.Equal(60, cashRefundAmount);
            Assert.Equal(30, paymentPlan.OustandingBalance());
        }
    }
}
